#!/system/bin/sh
# Please don't hardcode /magisk/modname/... ; instead, please use $MODDIR/...
# This will make your scripts compatible even if Magisk change its mount point in the future
MODDIR=${0%/*}

# This script will be executed in late_start service mode
# More info in the main Magisk thread

/system/xbin/dnsmasq -C /etc/dnsmasq.conf -x /data/local/dnsmasq.pid &
# Use OverrideDNS app and set DNS addresses to 127.0.0.1:5353

#iptables -t nat -A OUTPUT -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:5353
#iptables -t nat -A OUTPUT -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:5353
#ip6tables -t nat -A OUTPUT -p tcp --dport 53 -j DNAT --to-destination [::1]:5353
#ip6tables -t nat -A OUTPUT -p udp --dport 53 -j DNAT --to-destination [::1]:5353
